"""
PySA - gui_widgets.py

Window setup for the GUI.

Copyright (C) 2022-2023  Proton Fox

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""


from PyQt5.QtWidgets import (QApplication, QWidget, QLabel, QHBoxLayout, QVBoxLayout, QPushButton,\
                             QSpinBox, QDoubleSpinBox, QGroupBox, QLCDNumber, QCheckBox, QComboBox)
from PyQt5 import (QtGui, QtCore)
import pyqtgraph as pg
import numpy as np

from serial.tools import list_ports

from pysa import *


class MainWindow(QWidget):
    """Main program window class"""
    def __init__(self):
        QWidget.__init__(self)
        
        # Data definitions
        self.run = True
        self.freq = None
        self.meas = None
        self.ptr = 0
        self.i_mkr = 0
        
        # Widget definitions
        self.graph = pg.GraphicsLayoutWidget()
        self.p = self.graph.addPlot()
        self.curve = self.p.plot()
        self.mkr_disp = self.p.plot(symbol='+')
        self.p.setRange(yRange=(0,255), padding=0)
        self.apply_bt = QPushButton("Apply")
        self.fstart_setting = QDoubleSpinBox()
        self.fend_setting = QDoubleSpinBox()
        self.steps_setting = QSpinBox()
        self.mkr_enable = QCheckBox("Use marker")
        self.mkr_freq = QDoubleSpinBox()
        self.mkr_meas = QLCDNumber()
        
        sweep_settings = QGroupBox("Sweep settings")
        marker = QGroupBox("Marker")
        label_fstart = QLabel("Start")
        label_fend = QLabel("End")
        label_steps = QLabel("Steps")
        label_mkr_freq = QLabel("Marker frequency")
        label_mkr_meas = QLabel("Measure")
        
        # Widget setup
        self.fstart_setting.setRange(35, 4400)
        self.fstart_setting.setValue(98)
        self.fstart_setting.setSuffix(" MHz")
        self.fend_setting.setRange(35, 4400)
        self.fend_setting.setValue(102)
        self.fend_setting.setSuffix(" MHz")
        self.steps_setting.setRange(10, 500)
        self.steps_setting.setValue(100)
        self.mkr_freq.setSuffix(" MHz")
        
        # Grid
        self.p.showGrid(x = True, y = True, alpha = 0.3)
        self.p.getAxis('left').setPen('g', style=QtCore.Qt.DashLine)
        self.p.getAxis('right').setPen('g', style=QtCore.Qt.DashLine)
        self.p.getAxis('bottom').setPen('g', style=QtCore.Qt.DashDotLine)
        
        # Layout setup
        # layout : global layout, containing the scope plot
        # | settings_layout : right side panel, for all the settings
        # | | sweep_settings_layout : inside the 'sweep_settings' box
        # | | marker_layout : inside the 'marker' box
        layout = QHBoxLayout() # layouts definitions
        settings_layout = QVBoxLayout()
        sweep_settings_layout = QVBoxLayout()
        marker_layout = QVBoxLayout()
        
        layout.addWidget(self.graph) # widgets in the main layout
        
        settings_layout.addWidget(sweep_settings) # widgets in the settings layout
        settings_layout.addWidget(marker)
        
        sweep_settings_layout.addWidget(label_fstart) # widgets in the sweep settings layout
        sweep_settings_layout.addWidget(self.fstart_setting)
        sweep_settings_layout.addWidget(label_fend)
        sweep_settings_layout.addWidget(self.fend_setting)
        sweep_settings_layout.addWidget(label_steps)
        sweep_settings_layout.addWidget(self.steps_setting)
        sweep_settings_layout.insertSpacing(6, 100)
        sweep_settings_layout.addWidget(self.apply_bt)

        marker_layout.addWidget(self.mkr_enable) # widgets in the markers layout
        marker_layout.addWidget(label_mkr_freq)
        marker_layout.addWidget(self.mkr_freq)
        marker_layout.insertSpacing(6, 100)
        marker_layout.addWidget(label_mkr_meas)
        marker_layout.addWidget(self.mkr_meas)
        
        sweep_settings.setLayout(sweep_settings_layout) # put everything together
        marker.setLayout(marker_layout)
        layout.addLayout(settings_layout)
        self.setLayout(layout)
        
        # Events connections
        self.apply_bt.clicked.connect(self.cmd_apply)
        self.mkr_freq.valueChanged.connect(self.cmd_mkr)
        
        # Additional window decorations
        self.setWindowIcon(QtGui.QIcon('./media/logo.png'))
        self.setWindowTitle("PySA - Spectrum Analyzer GUI")
    
    def set_sweep_param(self, start, end, steps):
        """Sets the current sweep parameters"""
        self.freq = np.linspace(start, end, steps)
        self.meas = np.zeros(self.freq.size)
        
    def update(self, sa):
        """Queries measurements from the spectrum analyzer and updates the graphic window."""

        # display update
        sa.set_frequency(self.freq[self.ptr])
        sa.current_measure()
        self.meas[self.ptr] = sa.meas
        self.curve.setData(self.freq/1e+6, self.meas)
        QApplication.processEvents()
        
        self.ptr += 1
        
        if self.ptr >= self.freq.size:
            self.ptr = 0

        # marker measurement & display update
        if self.mkr_enable.isChecked():
            self.mkr_meas.display(self.meas[self.i_mkr])
            self.mkr_disp.setData([self.freq[self.i_mkr]/1e+6], [self.meas[self.i_mkr]])
        else:
            self.mkr_disp.clear()

    
    def closeEvent(self, event):
        """Called when the window is closed - exits the program"""
        self.run = False
    
    def cmd_apply(self):
        """Called when the 'apply' button of the sweep settings is pressed - updates the sweep settings"""
        fstart = self.fstart_setting.value()
        fend = self.fend_setting.value()
        steps = self.steps_setting.value()
        
        if fend <= fstart:
            fend = fstart + 1
            self.fend_setting.setValue(fend)
        
        self.p.setTitle("LINSWEEP {} - {} MHz, {} MHz step".format(fstart, fend, round((fend-fstart)/steps, 2)))
        self.set_sweep_param(fstart*1e+6, fend*1e+6, steps)

        # change the marker frequency selector
        self.mkr_freq.setRange(fstart, fend)
        self.mkr_freq.setSingleStep((fend-fstart)/steps)
        self.mkr_freq.setValue(fstart)

    def cmd_mkr(self, event):
        """Called when the 'goto' button is pressed - updates the marker's frequency"""
        if self.mkr_enable.isChecked():
            fstart = self.fstart_setting.value()
            fend = self.fend_setting.value()
            f_mkr = self.mkr_freq.value()

            if not (f_mkr <= fend and f_mkr >= fstart):
                f_mkr = fstart
                self.mkr_freq.setValue(f_mkr)

            # search for measurement value index
            for i in range(0, self.freq.size):
                if self.freq[i]/1e+6 >= f_mkr:
                    self.i_mkr = i
                    break

class GeneratorWindow(QWidget):
    """Class for a RF generator window"""

    def __init__(self):
        QWidget.__init__(self)

        # Data definitions
        self.run = True
        self.refresh = True
        self.freq = 0

        # Widget definitions
        self.freq_selector = QDoubleSpinBox()
        self.set_freq_bt = QPushButton("Set frequency")
        self.off_bt = QPushButton("RF Off")

        # Widget setup
        self.freq_selector.setSuffix(" MHz")
        self.freq_selector.setRange(35, 4400)
        self.freq_selector.setValue(100)
        self.freq_selector.setDecimals(3)

        # Window additional decorations
        main_label = QLabel("Frequency")
        self.setWindowIcon(QtGui.QIcon('./media/logo.png'))
        self.setWindowTitle("PySA - RF generator")
        self.setFixedSize(440, 95)

        # Layout setup
        # | layout: common layout for the whole window
        # | | left_layout: left panel for the frequency setup
        # | | right_layout: right panel for the generator controls
        layout = QHBoxLayout()
        left_layout = QVBoxLayout()
        right_layout = QVBoxLayout()

        # Place widgets
        left_layout.addWidget(main_label)
        left_layout.addWidget(self.freq_selector)
        right_layout.addWidget(self.set_freq_bt)
        right_layout.addWidget(self.off_bt)

        # Set up the window
        layout.addLayout(left_layout)
        layout.addLayout(right_layout)
        self.setLayout(layout)

        # Connect events
        self.set_freq_bt.clicked.connect(self.cmd_set_frequency)
        self.off_bt.clicked.connect(self.cmd_rf_off)

    def cmd_set_frequency(self, event):
        """Applies the new frequency"""
        self.freq = self.freq_selector.value()
        self.refresh = True

    def cmd_rf_off(self, event):
        """Closes the RF output"""
        self.freq = 0
        self.refresh = True

    def closeEvent(self, event):
        """Called when the window is closed - exits the program"""
        self.run = False

    def update(self, sa):
        """Updates the device's frequency and current window"""
        if self.refresh:
            sa.set_output_frequency(self.freq * 1e+6)
            self.refresh = False

        QApplication.processEvents()


class ConnectWindow(QWidget):
    """Class for a connect window"""

    def __init__(self):
        QWidget.__init__(self)

        # Data definitions
        self.connected_device = False
        self.ports_list = []
        self.port = ""

        # Widget definitions
        self.combo = QComboBox()
        self.connect_bt = QPushButton("Connect")

        # Window decorations
        main_label = QLabel("First, please chose a device to connect ...")
        sub_label = QLabel("PySA is designed to work with LTDZ 35-4400 USB spectrum analyzers.")
        self.setWindowIcon(QtGui.QIcon('./media/logo.png'))
        self.setWindowTitle("PySA - Please connect a device")
        self.setFixedSize(520, 180)

        # Layout setup
        # | layout: common layout for the whole window
        # | | sub_layout: layout containing the control widgets
        layout = QVBoxLayout()
        sub_layout = QHBoxLayout()

        # Place widgets
        layout.addWidget(main_label)
        layout.addWidget(sub_label)
        sub_layout.addWidget(self.combo)
        sub_layout.addWidget(self.connect_bt)

        # Set up the window
        layout.addLayout(sub_layout)
        self.setLayout(layout)

        self.list_ports()

        # Connect events
        self.combo.activated[str].connect(self.cmd_choose_port)
        self.connect_bt.clicked.connect(self.cmd_connect)

    def list_ports(self):
        ports = list_ports.comports(include_links=True)
        for prt in ports:
            boxitem = prt.device + " - " + prt.description
            self.ports_list.append(prt.device)
            self.combo.addItem(boxitem)


    def cmd_connect(self):
        self.connected_device = True
        self.close()

    def cmd_choose_port(self, text):
        self.port = self.ports_list[self.combo.currentIndex()]
