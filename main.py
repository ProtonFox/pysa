from pysa import *
import matplotlib.pyplot as plt
import numpy as np

sa = SpectrumAnalyzer("/dev/ttyUSB0")

meas = []
freq = np.linspace(92.5e+6, 97.5e+6, 800)

for f in freq:
    sa.set_frequency(f)
    sa.current_measure()
    meas.append(sa.meas)


sa.close()
plt.plot(freq, meas)
plt.show()
