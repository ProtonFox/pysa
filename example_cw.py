"""
PySA - example_cw.py

This example shows how to use the PySA scripting commands to use the
LTDZ 35-4400 MHz RF output port.

Copyright (C) 2022  Proton Fox

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""


from pysa import *

# The `port` variable is the path to the device's port.
# You may have to change it depending on your setup.
# The function `serial.tools.list_ports.comports()` may be useful.

port = "/dev/ttyUSB0"

sa = SpectrumAnalyzer(port) # connect to a device

sa.set_output_frequency(35e+6) # set the output frequency (Hz)

input("Press enter to leave ...")

sa.set_output_frequency(0) # turn off the RF generator

sa.close()
