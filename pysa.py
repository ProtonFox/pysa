"""
PySA - pysa.py

Python library for LTDZ 35-4400 MHz spectrum analyzer control.

Copyright (C) 2022-2023  Proton Fox

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""


import serial

### Control bytes definition
# Protocol documentation available at :
# https://www.tspi.at/2020/09/06/nwtprotocol.html

BEGIN = b'\x8f'
# LINSCAN = b'\x77'
# LOGSCAN = b'\x78'
FREQ = b'\x66'
CMEAS = b'\x6d'
CALREF = b'\x65'
VERS = b'\x76'
LOGSCAN2 = b'\x61'
LINSCAN2 = b'\x62'


### Common parameters definition

BAUDRATE = 57600
TIMEOUT = 2


### Main device class

class SpectrumAnalyzer:
    def __init__(self, port):
        """Class for a spectrum analyzer device. Port address must be specified."""
        self.meas = 0
        self.ser = None
        
        try:
            self.ser = serial.Serial(port, BAUDRATE, timeout=TIMEOUT)
            
            print("Successfully connected to the device, firmware version " + str(self.query_version()))
        except serial.serialutil.SerialException:
            print("ERROR - Unavailable device at " + port)
            print("Check device connection, port and try again.")

    def query_version(self):
        """Returns the device's version if it is connected"""
        if self.ser != None:
            self.ser.write(BEGIN + VERS)
            return int.from_bytes(self.ser.read(1), 'big')
        else:
            print("ERROR - Device is not connected.")
            return -1
    
    def set_frequency(self, freq):
        """Sets the current VFO frequency (Hz)."""
        self.ser.write(BEGIN + FREQ + number_format(freq, 9))

    def set_output_frequency(self, freq):
        """Sets the current RF output frequency (Hz)."""
        self.ser.write(BEGIN + FREQ + number_format(freq/10, 9))

    def log_scan(self, freq, dfreq, n, delay=10):
        """Performs a log scan from freq (Hz). Step width dfreq in Hz, n samples.
        Optional delay between measurements in microseconds."""

        freq_enc = number_format(freq, 8)
        dfreq_enc = number_format(dfreq, 8)
        n_enc = number_format(n, 4)
        delay_enc = number_format(delay, 3)

        print(BEGIN + LOGSCAN2 + freq_enc + dfreq_enc + n_enc + delay_enc)

        self.ser.write(BEGIN + LOGSCAN2 + freq_enc + dfreq_enc + n_enc + delay_enc)

        return self.read(n*4)


    def write(self, comm):
        """Sends a raw bytes sequence to the device. For test or special purposes only."""
        self.ser.write(comm)

    def read(self, n):
        """Reads n raw bytes from the device. For test or special purposes only."""
        return self.ser.read(n)
    
    def current_measure(self):
        """Queries the current measurement and returns the corresponding raw ADC sample value."""
        self.ser.write(BEGIN + CMEAS)
        self.meas = self.ser.read(4)[0]
    
    def close(self):
        """Closes the connection with the device."""
        self.ser.close()


### Useful auxiliary functions

def number_format(number, n):
    """Formats a number to a n-sized bytes chain"""
    chain = str(int(number))
    size = len(chain)
    
    if size < n: # zero-padding if size < n ...
        chain = "0" * (n - size) + chain
    elif size > n: # otherwise we crop the chain
        chain = chain[0:n]
    
    return bytes(chain, "ascii")
