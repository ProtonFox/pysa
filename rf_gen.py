"""
PySA - rf_gen.py

A minimalist GUI for using the LTDZ as an RF generator.

Copyright (C) 2022-2023  Proton Fox

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""


from PyQt5.QtWidgets import QApplication

from pysa import *
from gui_widgets import ConnectWindow, GeneratorWindow


class AppController:
    def __init__(self):
        self.sa = None

    def show_connect_window(self):
        self.conn = ConnectWindow()
        self.conn.show()

    def show_main_window(self):
        self.sa = SpectrumAnalyzer(self.conn.port)
        self.conn.close()

        # Main window setup
        self.main = GeneratorWindow()

        # Main loop
        while self.main.run:
            self.main.show()
            self.main.update(self.sa)

        self.sa.set_output_frequency(0)
        self.sa.close()


app = QApplication.instance()
if not app:
    app = QApplication([])

controller = AppController()
controller.show_connect_window()

app.exec_()

if controller.conn.connected_device:
    controller.show_main_window()
