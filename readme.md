PySA - A Python Interface for the LTDZ 35-4400 MHz spectrum analyzers
=====================================================================

![](./media/logo.svg)

_(may probably work with its siblings such as the D6 ?)_

Introduction
------------

This is an interface tool written in Python intended to be used with LTDZ 35-4400 MHz spectrum analyzers, in replacement of the rather old and unsuited software Lin/WinNWT. As I couldn't find a suitable alternative for Linux, I decided to create my own interface.

It consists of two parts: a lightweight library allowing simple communication with the spectrum analyzer, and a graphic user interface based on it. The library can also be used for scripting measurements.

This work is published freely, in the terms of the GNU GPL v2 public license. See `LICENSE` for more information.

Dependencies
------------

It is recommended to use pip to install dependencies and keep them up to date.

* Python 3
* Numpy (`pip install numpy`)
* PySerial (`pip install pyserial`)
* PyQt5 (`pip install PyQt5`) - mandatory for the graphic user interface
* pyqtgraph (`pip install pyqtgraph`) - mandatory for the graphic user interface

Use of the graphic interface
----------------------------

![](./media/screenshot.jpg)

The GUI is intended to be simple, intuitive, with a "lab bench instrument-like" look and feel. Unlike other software, parameters can be quickly typed and are specified in MHz, reducing the number of digits and simplifying operation. It features:

* Linear sweep from 35 MHz to 4.4 GHz;
* Markers for measurements;
* Logarithmic sweep from 35 MHz to 4.4 GHz (planned!);
* Support for minimal scalar network analysis with the embedded tracking generator (planned!);
* CW RF generation through the RF output port.

To use the graphic user interface, run `gui.py`. Make sure your unit has been plugged in first. You will be prompted to choose the serial port where your device is connected, then you will be able to control it with PySA's GUI.

Use of the RF generator feature
-------------------------------

This **experimental** GUI allows you to use the LTDZ 35-4400 as an RF generator. It works in a similar way as the Spectrum Analysis tool.

To use it, run `rf_gen.py`. Make sure your unit has been plugged in first. You will be prompted to choose the serial port where your device is connected, then you will be able to control it with PySA's GUI. You can set the frequency using the selector, and apply the changes using the "Set frequency" button. You can also quickly disable the RF output using the "RF Off" button. Clicking the "Set frequency" button afterwards will restore it.

Use of the standalone library
-----------------------------

This library will not include complicated features but allows an easy interface to the spectrum analyzer, with the minimal functionalities needed.

Currently, an example showing the RF output feature can be found in `example_cw.py`.

See `pysa.py` for further details.

Thanks
------

Special thanks to T. Spielauer (https://www.tspi.at) for providing useful documentation about the communication protocol.
